
load '/home/kenneth/5386f807-kenneth.cheston-battleship/lib/board.rb'
load '/home/kenneth/5386f807-kenneth.cheston-battleship/lib/player.rb'

class BattleshipGame

  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Ken"), board = Board.new)
    @player = player
    @board = board
  end

  def play
    i = 0
    puts "Welcome to Battleship Game Play."
    board.randomize
    while i < (board.size ** 2)
      play_turn
      return puts "You won!" if game_over?
      i += 1
    end
  end

  def attack(position)
    row, column = position
    board[position] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    board.display
    move = player.get_play
    board.hit?(move)
    attack(move)
  end
end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
