
load '/home/kenneth/5386f807-kenneth.cheston-battleship/lib/board.rb'
load '/home/kenneth/5386f807-kenneth.cheston-battleship/lib/player.rb'

load '/home/kenneth/5386f807-kenneth.cheston-battleship/lib/computer.rb'

class BattleshipGame

  attr_reader :board, :player, :player_02, :board_02

  def initialize(player = HumanPlayer.new("Ken"), board = Board.new)
    @player = player
    @board = board
  end

  def play
    setup
    setup_comp
    i = 0

    puts "Welcome to Battleship Game Play."
    while i < (board.size ** 2)
      play_turn(@board, @player, nil)
      return puts "You won!" if game_over?
      play_turn(board_02, player_02, board_02.size)
      return puts "You Lost :(" if game_over? == true
      i += 1
    end
  end

  def attack(position, board = @board)
    row, column = position
    board[position] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won? || board_02.won?
  end

  def valid_move?(move)
    range = (0...board.size).to_a
    if !(range.include?(move[0]) && range.include?(move[1]))
      return false
    elsif !(board[move] == nil)
      return false
    else true
    end
  end

  def play_turn(board = @board, player = @player, range = nil)
    board.display
    move = player.get_play(range)
    while !(valid_move?(move))
      move = player.get_play(range)
    end
    board.hit?(move)
    attack(move, board)
  end


  def valid_setup?(move, direction, ship)
    @indices = []

    if direction == "right"
      idx = 1
      last_idx = [move[0], (move[1] + ship)]
      (move[idx]..last_idx[idx]).to_a.each { |num| @indices << [move[0], num] }
    elsif direction == "left"
      idx = 1
      last_idx = [move[0], (move[1] - ship)]
      (last_idx[idx]..move[idx]).to_a.each { |num| @indices << [move[0], num] }
    elsif direction == "up"
      idx = 0
      last_idx = [(move[0] - ship), move[1]]
      (last_idx[idx]..move[idx]).to_a.each { |num| @indices << [num, move[1]] }
    elsif direction == "down"
      idx = 0
      last_idx = [(move[0] + ship), move[1]]
      (move[idx]..last_idx[idx]).to_a.each { |num| @indices << [num, move[1]] }
    else return false
    end

    @indices.all? { |pos| valid_move?(pos) }
  end

  def setup_ship(ship, board, player = @player)

    while true
      move = player.get_play(10)
      puts "Direction? (i.e. left, right, up, down)"
      direction = player.get_direction

      if !(valid_setup?(move, direction, ship))
        puts "Invalid Placement. Try Again."
      else break
      end
    end
    board.place_ship(@indices)
  end

  def setup
    puts "Welcome to Battlefield. Setup phase will now begin. Select starting point and direction for each ship when prompeted (directions: up, down, left, & right). Example: '[3,4] right'"

    puts "\nPlace Aircraft Carrier (5x1) by selecting starting spot."
    board_02.display
    setup_ship(4, board_02)
    board_02.display

    puts "\nPlace Battleship (4x1) by selecting starting spot."

    setup_ship(3, board_02)
    board_02.display

    puts "\nPlace Submarine (3x1) by selecting starting spot."

    setup_ship(2, board_02)
    board_02.display

    puts "\nPlace Destroyer (3x1) by selecting starting spot."

    setup_ship(2, board_02)
    board_02.display

    puts "\nPlace Patrol Boat (2x1) by selecting starting spot."

    setup_ship(1, board_02)
    board_02.display
  end

  def setup_comp
    ships = [1,2,2,3,4]
    ships.each {|ship| setup_ship(ship, board, @player_02) }
  end
end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
