class HumanPlayer

  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play(range = 0)
    print "\n\n Pick a position. (i.e - 43)"
    move = gets.chomp.split("").map { |num| num.to_i }
  end

  def get_direction
    gets.chomp
  end

end
