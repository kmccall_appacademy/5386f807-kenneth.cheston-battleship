class Board

  attr_reader :grid



  def initialize(grid = Board.default_grid)
    @grid = grid
    rand(size).times { place_random_ship }
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def [](position)
    row, column = position
    grid[row][column]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  def count
    counter = 0
    grid.each do |row|
      row.each { |position| counter += 1 if position == :s }
    end
    counter
  end

  def hit?(pos)
    if self[pos] == :s
      puts "Hit!"
    else puts "Miss!"
    end
  end

  def empty?( position = 0 )
    if position == 0
      return true if count == 0
      return false
    else
      ( grid[position[0]] )[position[1]] == nil
    end
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise "Board is full." if full?
    grid[rand(size)][rand(size)] = :s
  end

  def display
    render = { nil => "  ", :s => ":s", :x => ":x" }

    board = "  "
    index = [0...6]

    grid.each_with_index {|num, idx| board << "#{idx}  "}

    grid.each_with_index do |row, idx|
      board << "\n#{idx}"
      row.each_with_index do |pos, idx_2|
        board << render[pos]
        board << "|" unless idx_2 == row.length - 1
      end
    end
    print board
  end

  def won?
    empty?
  end

  def size
    grid.length
  end
end
