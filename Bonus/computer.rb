

class ComputerPlayer

  def get_play(range)
    [rand(range - 1), rand(range - 1)]
  end

  def get_direction
    direction = ["left", "right", "up", "down"]
    direction.shuffle[0]
  end
end
